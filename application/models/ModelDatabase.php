<?php
/**
 * Modelo para realizar consultas a la base de datos
 * 
 * @author Jaime Montoya <innovasistemas@gmail.com>
 * @abstract Proyecto para gestionar usuarios
 * @copyright (c) 201/, Jaime Montoya
 * @creation-date 20/11/2018
 * @update-date 20/11/2018
 * @version 1.0
 * @class ModelDatabase. Modelo principal de la aplicación
 * @link http://localhost/users-codeigniter/index.php/begin/read
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class ModelDatabase extends CI_Model
{
    // Constructor de la clase
    public function __construct()
    {
        parent::__construct();
    }


    // Destructor de la clase
    public function __destruct()
    {

    }

    // Método index
    public function index()
    {

    }


    public function read($table, $fieldsArray, $pagination, $segment)
    {
    	$this->db->select($fieldsArray);
    	$this->db->from($table);
        $this->db->limit($pagination, $segment);
    	return $this->db->get();
    }


    public function totalRecords($table)
    {
        return $this->db->get($table)->num_rows();
    }


    public function delete($table, $arrayClause)
    {
        return $this->db->delete($table, $arrayClause);
    }


    public function find($table, $field, $value)
    {
        $this->db->where("UPPER($field)", strtoupper($value));
        return $this->db->get($table);
    }


    public function update($table, $record, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update($table, $record);
    }


    public function insert($table, $record)
    {
        return $this->db->insert($table, $record);
    }


    public function validateLogin($arrayRecord)
    {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where($arrayRecord['field'], $arrayRecord['data']);
        $this->db->where('clave', $arrayRecord['password']);

        return $this->db->get();
    }

}
